defmodule Gateway.Router do
  use Plug.Router
  require Logger

  plug Plug.Logger, log: :debug
  plug Plug.Parsers, parsers: [:json],
                     pass:  ["text/*"],
                     json_decoder: Poison
  plug :match
  plug :dispatch

  def init(_opts) do
    :ets.new(:gateway_routes, [:set, :protected, :named_table])
  end

  put "/mngt/route" do

    Logger.debug "#{inspect conn, pretty: true}"

    Logger.debug "#{inspect conn.body_params["route"], pretty: true}"

    # :ets.insert(:gateway_routes, {route, {host, port}})
    send_resp(conn, 200, %{success: true, payload: []})
  end

  get "/mngt/routes" do
    key_value_pair = :ets.tab2list(:gateway_routes)
    send_resp(conn, 200, Enum.map(key_value_pair, fn ({key, _}) -> key end))
  end

  match _, do: proxy_target(conn)

  defp proxy_target(conn) do
    routes = [
      {"/api/auth", ["localhost", 2001]},
      {"/api/client", ["localhost", 2002]},
    ]
    target = Enum.find_value(
      routes,
      [nil, nil],
      fn({route, target}) -> if String.starts_with?(conn.request_path, route), do: target, else: [nil, nil] end
    )

    Logger.debug "#{inspect target}"
    proxy(conn, target)
  end

  defp proxy(conn, [nil, nil]), do: send_resp(conn, 502, "Bad Gateway")

  defp proxy(conn, [host, port]) do

    Logger.debug "body: #{inspect conn, pretty: true}"
    {:ok, body, conn} = Plug.Conn.read_body(conn, length: 1_000_000)

    method = conn.method |> String.downcase |> String.to_atom
    url = "http://" <> host <> ":" <> to_string(port) <> conn.request_path
    {status, resp} = HTTPoison.request(method, url, body, conn.req_headers)

    Logger.debug "status: #{status}"

    %HTTPoison.Response{status_code: status_code, body: body, headers: headers} = resp
    Logger.debug "status_code: #{status_code}"
    Logger.debug "headers: #{inspect headers, pretty: true}"
    Logger.debug "body: #{inspect body, pretty: true}"

    {_, content_type} = List.keyfind(headers, "Content-Type", 0)
    # Logger.debug "content-type: #{inspect content_type}"

    conn
    |> put_resp_content_type(content_type)
    |> send_resp(200, body)
  end
end

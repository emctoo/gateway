#!/usr/bin/env python
# coding: utf8

import sanic
from sanic import response
import jwt
import redis

app = sanic.Sanic()
app.config['user_counter'] = 0

redis_client = redis.StrictRedis()

@app.post('/api/auth/login')
async def login(req):
  username = req.json['username']
  req.app.config['user_counter'] += 1
  token = jwt.encode({'user_id': req.app.config['user_counter']}, key='theNewSecret')
  return response.json({
    'success': True,
    'payload': {
      'user_id': req.app.config['user_counter'],
      'username': username,
      'token': token,
    }
  })

@app.get('/api/auth/<user_id>')
async def clientById(req, user_id):
  return response.json({'success': True, 'payload': ['a', 'b']})

@app.get('/api/auth')
async def auth(req):
  return response.json({'success': True, 'payload': []})

@app.put('/api/auth')
async def insertClient(req):
  print(req.json)
  return response.json({'success': True, 'payload': 'ok'})

@app.get('/api/client/health')
async def checkHealth(req):
  return response.json({'success': True, 'payload': 'ok'})


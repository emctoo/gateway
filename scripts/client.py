#!/usr/bin/env python
# coding: utf8

import sanic
from sanic import response

app = sanic.Sanic()

@app.get('/api/client/<id>')
async def clientById(req, id):
  return response.json({'success': True, 'payload': ['a', 'b']})

@app.post('/api/client')
async def insertClient(req):
  print(req.json)
  return response.json({'success': True, 'payload': 'ok'})

@app.get('/api/client/health')
async def checkHealth(req):
  return response.json({'success': True, 'payload': 'ok'})


defmodule Gateway.MixProject do
  use Mix.Project

  def project do
    [
      app: :gateway,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      application: [:cowboy, :plug, :httpoison] ++
        (if Mix.env() == :prod, do: [], else: [:remix]),
      extra_applications: [:logger],
      mod: {Gateway, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:distillery, "~> 1.5", runtime: false},
      {:plug, "~> 1.5.1"},
      {:cowboy, "~> 2.4.0"},
      {:jason, ">= 1.0.0"},
      {:httpoison, "~> 1.0"},
      {:remix, "~> 0.0.1", only: :dev},
    ]
  end
end
